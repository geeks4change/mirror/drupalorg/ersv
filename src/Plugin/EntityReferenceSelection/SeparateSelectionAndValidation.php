<?php

namespace Drupal\ersv\Plugin\EntityReferenceSelection;

use Drupal\ajax_dependency\AjaxDependency;
use Drupal\Component\Utility\Html;
use Drupal\Core\Database\Query\SelectInterface;
use Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginBase;
use Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManagerInterface;
use Drupal\Core\Entity\EntityReferenceSelection\SelectionWithAutocreateInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'separate selection and validation' plugin.
 *
 * @EntityReferenceSelection(
 *   id = "ersv",
 *   label = @Translation("Separate selection and validation"),
 *   group = "ersv",
 *   weight = 0
 * )
 */
class SeparateSelectionAndValidation extends SelectionPluginBase implements ContainerFactoryPluginInterface, SelectionWithAutocreateInterface {

  /**
   * Selection plugin.
   *
   * @var null | \Drupal\Core\Entity\EntityReferenceSelection\SelectionInterface | \Drupal\Component\Plugin\DependentPluginInterface | \Drupal\Component\Plugin\ConfigurableInterface
   */
  protected $selectionPlugin;

  /**
   * Validation plugin.
   *
   * @var null | \Drupal\Core\Entity\EntityReferenceSelection\SelectionInterface | \Drupal\Component\Plugin\DependentPluginInterface | \Drupal\Component\Plugin\ConfigurableInterface
   */
  protected $validationPlugin;

  /**
   * @var \Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManagerInterface
   */
  protected $selectionPluginManager;

  /**
   * {@inheritdoc}
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.entity_reference_selection')
    );
  }

  /**
   * {@inheritdoc}
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, SelectionPluginManagerInterface $selectionPluginManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->selectionPluginManager = $selectionPluginManager;

    if (!empty($configuration['selection']['settings']['handler'])) {
      $this->selectionPlugin = $this->createInstance($configuration['selection']['settings']['handler'], $configuration['selection']['settings']['handler_settings'] ?? []);
    }
    if (!empty($configuration['validation']['settings']['handler'])) {
      $this->validationPlugin = $this->createInstance($configuration['validation']['settings']['handler'], $configuration['validation']['settings']['handler_settings'] ?? []);
    }
  }

  /**
   * @param $pluginId
   * @param $configuration
   *
   * @return object
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   *
   * @see \Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManager::getSelectionHandler
   * @see \Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem::fieldSettingsForm
   */
  protected function createInstance($pluginId, $configuration = []) {
    $configuration = $configuration ?: [];
    $configuration += [
      'target_type' => $this->getConfiguration()['target_type'],
      'handler' => $pluginId,
      'entity' => NULL,
    ];
    $instance = $this->selectionPluginManager->createInstance($pluginId, $configuration);
    return $instance;
  }

  /**
   * @inheritDoc
   */
  public function defaultConfiguration() {
    // We need this strange settings key in between for as plugin validation
    // does not use subform state and expects such a key.
    return [
        'selection' => [
          'settings' => [
            'handler' => NULL,
            'handler_settings' => [],
          ],
        ],
        'validation' => [
          'settings' => [
            'handler' => NULL,
            'handler_settings' => [],
          ],
        ],
        'target_bundles' => [],
      ] + parent::defaultConfiguration();
  }

  /**
   * @inheritDoc
   */
  public function buildConfigurationForm(array $form, FormStateInterface $formState) {
    $form = parent::buildConfigurationForm($form, $formState);
    $configuration = $this->getConfiguration();
    $handlerOptions = $this->handlerOptions();

    $form['selection'] = [
      '#type' => 'fieldset',
      '#title' => t('Selection'),
      '#description' => t('Determines which items can be selected.'),
      '#description_display' => 'before',
    ];
    $form['selection']['settings']['handler'] = [
      '#type' => 'select',
      '#title' => t('Handler'),
      '#options' => $handlerOptions,
      '#default_value' => $configuration['selection']['settings']['handler'],
      '#required' => TRUE,
    ];
    $form['selection']['settings']['handler_settings'] = [];
    if ($this->selectionPlugin) {
      $form['selection']['settings']['handler_settings'] = $this->selectionPlugin->buildConfigurationForm([], $formState);
    }
    AjaxDependency::dependsOn($form['selection']['settings']['handler'], $form['selection']['settings']['handler_settings'], $formState);

    $form['validation'] = [
      '#type' => 'fieldset',
      '#title' => t('Validation'),
      '#description' => t('Determines which items are valid, even if they can no be selected anymore. Also, if applicable, which items can be created.'),
      '#description_display' => 'before',
    ];
    $form['validation']['settings']['handler'] = [
      '#type' => 'select',
      '#title' => t('Handler'),
      '#options' => $handlerOptions,
      '#default_value' => $configuration['validation']['settings']['handler'],
      '#required' => TRUE,
    ];
    $form['validation']['settings']['handler_settings'] = [];
    if ($this->validationPlugin) {
      $form['validation']['settings']['handler_settings'] = $this->validationPlugin->buildConfigurationForm([], $formState);
    }
    AjaxDependency::dependsOn($form['validation']['settings']['handler'], $form['validation']['settings']['handler_settings'], $formState);

    return $form;
  }

  /**
   * Get selection plugin options.
   *
   * @see \Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem::fieldSettingsForm
   *
   * @return array
   */
  protected function handlerOptions() {
    // Get all selection plugins for this entity type.
    $targetTypeId = $this->getConfiguration()['target_type'] ?? NULL;
    $selection_plugins = $this->selectionPluginManager->getSelectionGroups($targetTypeId);
    $handlers_options = [];
    foreach (array_keys($selection_plugins) as $selection_group_id) {
      if ($selection_group_id === 'ersv') {
        continue;
      }
      // We only display base plugins (e.g. 'default', 'views', ...) and not
      // entity type specific plugins (e.g. 'default:node', 'default:user',
      // ...).
      if (array_key_exists($selection_group_id, $selection_plugins[$selection_group_id])) {
        $handlers_options[$selection_group_id] = Html::escape($selection_plugins[$selection_group_id][$selection_group_id]['label']);
      }
      elseif (array_key_exists($selection_group_id . ':' . $targetTypeId, $selection_plugins[$selection_group_id])) {
        $selection_group_plugin = $selection_group_id . ':' . $targetTypeId;
        $handlers_options[$selection_group_plugin] = Html::escape($selection_plugins[$selection_group_id][$selection_group_plugin]['base_plugin_label']);
      }
    }
    return $handlers_options;
  }

  /**
   * {@inheritdoc}
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $formState) {
    parent::validateConfigurationForm($form, $formState);

    // Wuuhaa, form: settings.handler.handler_settings,
    // values: settings.handler_settings
    // and $form = $completeForm['settings']
    $ourForm =& $form['handler']['handler_settings'];
    foreach (['selection', 'validation'] as $what) {
      if ($selectionPluginId = $formState->getValue(['settings', 'handler_settings', $what, 'settings', 'handler'])) {
        /** @var \Drupal\Core\Entity\EntityReferenceSelection\SelectionInterface $plugin */
        $subformState = SubformState::createForSubform($ourForm[$what]['settings']['handler_settings'], $formState->getCompleteForm(), $formState);
        $plugin = $this->createInstance($selectionPluginId);
        $plugin->validateConfigurationForm($ourForm[$what]['settings']['handler_settings'], $subformState);

        if ($what === 'validation') {
          // @see \Drupal\inline_entity_form\Plugin\Field\FieldWidget\InlineEntityFormSimple::isApplicable
          $targetBundles = $subformState->getValue('target_bundles');
          $formState->setValue(['settings', 'handler_settings', 'target_bundles'], $targetBundles);
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    $dependencies = parent::calculateDependencies();
    if ($this->selectionPlugin) {
      $dependencies += $this->selectionPlugin->calculateDependencies();
    }
    if ($this->validationPlugin) {
      $dependencies += $this->validationPlugin->calculateDependencies();
    }
    return $dependencies;
  }

  /**
   * {@inheritdoc}
   */
  public function entityQueryAlter(SelectInterface $query) {
    parent::entityQueryAlter($query);
    if ($this->validationPlugin) {
      $this->validationPlugin->entityQueryAlter($query);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getReferenceableEntities($match = NULL, $match_operator = 'CONTAINS', $limit = 0) {
    if ($this->selectionPlugin) {
      return $this->selectionPlugin->getReferenceableEntities($match, $match_operator, $limit);
    }
    else {
      return [];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function countReferenceableEntities($match = NULL, $match_operator = 'CONTAINS') {
    if ($this->selectionPlugin) {
      return $this->selectionPlugin->countReferenceableEntities($match, $match_operator);
    }
    else {
      return 0;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateReferenceableEntities(array $ids) {
    if ($this->validationPlugin) {
      return $this->validationPlugin->validateReferenceableEntities($ids);
    }
    else {
      return [];
    }
  }

  /**
   * {@inheritdoc}
   * @see \Drupal\Core\Entity\Element\EntityAutocomplete::validateEntityAutocomplete
   */
  public function createNewEntity($entity_type_id, $bundle, $label, $uid) {
    if ($this->validationPlugin instanceof SelectionWithAutocreateInterface) {
      return $this->validationPlugin->createNewEntity($entity_type_id, $bundle, $label, $uid);
    }
    else {
      // Apart from the PHPDoc contract, which mandates a nonnull entity, the
      // only core usage filters out unset entities.
      // @todo Change the contract upstream.
      return NULL;
    }
  }

  /**
   * {@inheritdoc}
   * @see \Drupal\Core\Entity\Plugin\Validation\Constraint\ValidReferenceConstraintValidator::validate
   */
  public function validateReferenceableNewEntities(array $entities) {
    if ($this->validationPlugin instanceof SelectionWithAutocreateInterface) {
      return $this->validationPlugin->validateReferenceableNewEntities($entities);
    }
    else {
      return [];
    }
  }
}
